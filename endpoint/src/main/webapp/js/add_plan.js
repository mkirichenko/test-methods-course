$(document).ready(function(){
    $("#create_plan_id").click(
        function() {
            var planName = $("#plan_name_id").val();
            var planDetails = $("#plan_details_id").val();
            var planFee = $("#plan_fee_id").val();

            // check fields
            if(planName =='') {
                $('input[type="text"]').css("border","2px solid red");
                $('input[type="text"]').css("box-shadow","0 0 3px red");
                alert("Name is empty");
            } else if (planDetails == '') {
                $('input[type="text"]').css("border","2px solid red");
                $('input[type="text"]').css("box-shadow","0 0 3px red");
                alert("Details is empty");
            } else if (planFee == '') {
                $('input[type="text"]').css("border","2px solid red");
                $('input[type="text"]').css("box-shadow","0 0 3px red");
                alert("Fee is empty");
            } else {
                $.post({
                    url: 'rest/create_plan',
                    headers: {
                        'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                                         	"name":planName,
                                            "details":planDetails,
                                            "fee":planFee
                                         })
                }).done(function(data) {
                     $.redirect('/endpoint/plans.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
                }).fail(function(data) {
                    alert(data.status + ' ' + data.responseText);
                });
            }
        }
    );
});