package ru.nsu.fit.endpoint.service.manager;

import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class PlanManager extends ParentManager {
    public PlanManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Plan. Ограничения:
     * name - длина не больше 128 символов и не меньше 2 включительно не содержит спец символов. Имена не пересекаются друг с другом;
    /* details - длина не больше 1024 символов и не меньше 1 включительно;
    /* fee - больше либо равно 0 но меньше либо равно 999999.
     */
    public Plan createPlan(Plan plan) {
        validatePlan(plan);
        return dbService.createPlan(plan);
    }

    public Plan updatePlan(Plan plan) {
        validatePlan(plan);
        return dbService.updatePlan(plan);
    }

    public void removePlan(UUID id) {
        dbService.deletePlan(id);
    }

    /**
     * Метод возвращает список планов доступных для покупки.
     */
    public List<Plan> getPlans(UUID customerId) {
        Customer customer = dbService.getCustomer(customerId);
        return dbService.getPlans().stream()
            .filter(plan -> plan.getFee() <= customer.getBalance())
            .collect(Collectors.toList());
    }

    public List<Plan> getPlans() {
        return dbService.getPlans();
    }

    private void validatePlan(Plan plan) {
        if (plan == null) {
            throw new IllegalArgumentException();
        }
        if (plan.getName() == null || plan.getName().length() < 3 || plan.getName().length() > 128) {
            throw new IllegalArgumentException("Invalid name length");
        }
        if (plan.getDetails() == null || plan.getDetails().length() < 2 || plan.getDetails().length() > 1024) {
            throw new IllegalArgumentException("Invalid details length");
        }
        if (plan.getFee() < 0 || plan.getFee() >= 1_000_000) {
            throw new IllegalArgumentException("Invalid fee amount");
        }
    }
}
