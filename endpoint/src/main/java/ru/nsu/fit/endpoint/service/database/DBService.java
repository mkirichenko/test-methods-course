package ru.nsu.fit.endpoint.service.database;

import jersey.repackaged.com.google.common.collect.Lists;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DBService {
    // Constants
    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, balance) values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String INSERT_SUBSCRIPTION = "INSERT INTO SUBSCRIPTION(id, customer_id, plan_id) values ('%s', '%s', '%s')";
    private static final String INSERT_PLAN = "INSERT INTO PLAN(id, name, details, fee) values ('%s', '%s', '%s', %s)";

    private static final String SELECT_CUSTOMER = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_CUSTOMERS = "SELECT * FROM CUSTOMER";

    private Logger logger;
    private static final Object generalMutex = new Object();
    private Connection connection;

    public DBService(Logger logger) {
        this.logger = logger;
        init();
    }

    public Customer createCustomer(Customer customerData) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createCustomer' was called with data: '%s'", customerData));

            customerData.setId(UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER,
                                customerData.getId(),
                                customerData.getFirstName(),
                                customerData.getLastName(),
                                customerData.getLogin(),
                                customerData.getPass(),
                                customerData.getBalance()));
                return customerData;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public void deleteCustomer(UUID id) {
        synchronized (generalMutex) {
        	logger.info("deleteCustomer " + id);
            try {
				PreparedStatement preparedStatement =
						connection.prepareStatement("DELETE FROM CUSTOMER WHERE id = ?");
				preparedStatement.setString(1, id.toString());
				int deletedCustomers = preparedStatement.executeUpdate();
				if (deletedCustomers == 0) {
					throw new IllegalArgumentException("No customer with id " + id);
				}
			} catch (SQLException e) {
                logger.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    public List<Customer> getCustomers() {
        synchronized (generalMutex) {
            logger.info("Method 'getCustomers' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_CUSTOMERS);
                List<Customer> result = Lists.newArrayList();
                while (rs.next()) {
                    Customer customerData = new Customer()
                            .setFirstName(rs.getString(2))
                            .setLastName(rs.getString(3))
                            .setLogin(rs.getString(4))
                            .setPass(rs.getString(5))
                            .setBalance(rs.getInt(6));

                    result.add(customerData);
                }
                return result;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public UUID getCustomerIdByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getCustomerIdByLogin' was called with data '%s'.", customerLogin));

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER,
                                customerLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public Plan createPlan(Plan plan) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createPlan' was called with data '%s'.", plan));

            plan.setId(UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_PLAN,
                                plan.getId(),
                                plan.getName(),
                                plan.getDetails(),
                                plan.getFee()));
                return plan;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public Plan updatePlan(Plan plan) {
        synchronized (generalMutex) {
			logger.info("updatePlan " + plan.getId());
			try {
				ResultSet resultSet = getPlanResultSet(plan.getId());
				if (resultSet.next()) {
					resultSet.updateString(2, plan.getName());
					resultSet.updateString(3, plan.getDetails());
					resultSet.updateInt(4, plan.getFee());
					resultSet.updateRow();
					return plan;
				} else {
					throw new IllegalArgumentException("No plan with id " + plan.getId());
				}
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
		}
    }

    public void deletePlan(UUID id) {
		synchronized (generalMutex) {
			logger.info("deletePlan " + id);
			try {
				PreparedStatement statement = connection.prepareStatement(
						"DELETE FROM PLAN WHERE id = ?");
				statement.setString(1, id.toString());
				int deletedPlans = statement.executeUpdate();
				if (deletedPlans == 0) {
					throw new IllegalArgumentException("No plan with id " + id);
				}
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
		}
    }

    public Plan getPlan(UUID id) {
    	synchronized (generalMutex) {
			logger.info("getPlan " + id);
			try {
				ResultSet resultSet = getPlanResultSet(id);
				if (resultSet.next()) {
					Plan plan = new Plan();
					plan.setId(UUID.fromString(resultSet.getString(1)));
					plan.setName(resultSet.getString(2));
					plan.setDetails(resultSet.getString(3));
					plan.setFee(resultSet.getInt(4));
					return plan;
				} else {
					throw new IllegalArgumentException("No plan with id " + id);
				}
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
		}
    }

    private ResultSet getPlanResultSet(UUID id) throws SQLException {
		PreparedStatement preparedStatement =
				connection.prepareStatement(
						"SELECT id, name, details, fee FROM PLAN WHERE id = ?",
						ResultSet.TYPE_FORWARD_ONLY,
						ResultSet.CONCUR_UPDATABLE);
		preparedStatement.setString(1, id.toString());
		return preparedStatement.executeQuery();
	}


	public Customer getCustomer(UUID customerId) {
		synchronized (generalMutex) {
			logger.info("getCustomer " + customerId);
			try {
				PreparedStatement statement = connection.prepareStatement(
						"SELECT id, first_name, last_name, login, pass, balance FROM CUSTOMER WHERE id = ?");
				statement.setString(1, customerId.toString());
				ResultSet resultSet = statement.executeQuery();
				if (resultSet.next()) {
					Customer customer = new Customer();
					customer.setId(customerId);
					customer.setFirstName(resultSet.getString(2));
					customer.setLastName(resultSet.getString(3));
					customer.setLogin(resultSet.getString(4));
					customer.setPass(resultSet.getString(5));
					customer.setBalance(resultSet.getInt(6));
					return customer;
				} else {
					throw new IllegalArgumentException("No customer with id " + customerId);
				}
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
		}
	}

	public List<Plan> getPlans() {
		synchronized (generalMutex) {
			logger.info("getPlans");
			try {
				PreparedStatement statement = connection.prepareStatement(
						"SELECT id, name, details, fee FROM PLAN");
				ResultSet resultSet = statement.executeQuery();
				ArrayList<Plan> plans = new ArrayList<>();

				while(resultSet.next()) {
					Plan plan = new Plan();
					plan.setId(UUID.fromString(resultSet.getString(1)));
					plan.setName(resultSet.getString(2));
					plan.setDetails(resultSet.getString(3));
					plan.setFee(resultSet.getInt(4));
					plans.add(plan);
				}
				return plans;
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
		}
	}

	public boolean isLoginUsed(String login) {
    	try (PreparedStatement statement = connection.prepareStatement("SELECT id FROM CUSTOMER WHERE login = ?")) {
			statement.setString(1, login);
			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(e);
    	}
	}

    private void init() {
        logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("MySQL JDBC Driver Registered!");

        try {

        	org.h2.Driver.load();
            connection = DriverManager
                    .getConnection(
                    	"jdbc:mysql://localhost:3306/testmethods?useUnicode=true",
						"user",
						"user");
        } catch (SQLException ex) {
            logger.error("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.error("Failed to make connection!");
        }
    }
}
