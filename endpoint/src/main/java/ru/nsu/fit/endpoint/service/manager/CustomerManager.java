package ru.nsu.fit.endpoint.service.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

public class CustomerManager extends ParentManager {
    public CustomerManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Customer. Ограничения:
     * Аргумент 'customerData' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же email;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать части login, firstName, lastName
     * money - должно быть равно 0.
     */
    public Customer createCustomer(Customer customer) {
        validateCustomer(customer);
        return dbService.createCustomer(customer);
    }

    /**
     * Метод возвращает список объектов типа customer.
     */
    public List<Customer> getCustomers() {
        return dbService.getCustomers();
    }

    public Customer getCustomer(UUID id) {
        return dbService.getCustomer(id);
    }


    /**
     * Метод обновляет объект типа Customer.
     * Можно обновить только firstName и lastName.
     */
    public Customer updateCustomer(Customer customer) {
        throw new NotImplementedException("Please implement the method.");
    }

    public void removeCustomer(UUID id) {
        dbService.deleteCustomer(id);
    }

    /**
     * Метод добавляет к текущему баласу amount.
     * amount - должен быть строго больше нуля.
     */
    public Customer topUpBalance(UUID customerId, int amount) {
        throw new NotImplementedException("Please implement the method.");
    }

    private void validateCustomer(Customer customer) {
        if (customer == null) {
            throw new IllegalArgumentException();
        }
        Pattern namePattern = Pattern.compile("^[A-ZА-Я][a-zа-я]+$");
        if (customer.getFirstName() == null || customer.getFirstName().length() < 2
            || customer.getFirstName().length() > 12 || !namePattern.matcher(customer.getFirstName()).matches()) {
            throw new IllegalArgumentException("Invalid first name");
        }
        if (customer.getLastName() == null || customer.getLastName().length() < 2
            || customer.getLastName().length() > 12 || !namePattern.matcher(customer.getLastName()).matches()) {
            throw new IllegalArgumentException("Invalid last name");
        }
        Pattern emailPattern = Pattern.compile("\\w+@\\w+\\.\\w+");
        if (customer.getLogin() == null || !emailPattern.matcher(customer.getLogin()).matches()) {
            throw new IllegalArgumentException("Invalid login");
        }
        if (dbService.isLoginUsed(customer.getLogin())) {
			throw new IllegalArgumentException("Login is used already");
		}
        if (customer.getPass() == null || customer.getPass().length() < 6
            || customer.getPass().length() > 12
            || customer.getPass().contains(customer.getFirstName())
            || customer.getPass().contains(customer.getLastName())
            || customer.getPass().contains(customer.getLogin())) {

            throw new IllegalArgumentException("Invalid password");
        }
        if (customer.getBalance() != 0) {
            throw new IllegalArgumentException("Invalid balance amount (must be zero)");
        }
    }
}
