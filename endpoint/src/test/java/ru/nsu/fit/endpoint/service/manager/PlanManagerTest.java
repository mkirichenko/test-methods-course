package ru.nsu.fit.endpoint.service.manager;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * Created by mikhail on 25/10/2018, 16:34;
 */

public class PlanManagerTest {

	private DBService dbService;

	private Logger logger;

	private PlanManager planManager;

	private Plan testPlan;

	private UUID existentUuid;

	private UUID nonexistentUuid;

	public PlanManagerTest() {
		this.dbService = mock(DBService.class);
		this.logger = mock(Logger.class);
		this.planManager = new PlanManager(dbService, logger);
	}

	@Before
	public void initTestPlan() {
		testPlan = new Plan();
		testPlan.setName("Plan name");
		testPlan.setDetails("Plan details");
		testPlan.setFee(100);

		existentUuid = UUID.fromString("6de0ce6b-8af2-4c27-8152-f82b46ec3a55");
		when(dbService.getPlan(eq(existentUuid))).thenReturn(testPlan);
		doNothing().when(dbService).deletePlan(eq(existentUuid));
		doThrow(new IllegalArgumentException()).when(dbService).deletePlan(eq(nonexistentUuid));
	}

	@Test
	public void createPlanSuccessfullyTest() {
		planManager.createPlan(testPlan);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createPlanShortNameTest() {
		testPlan.setName("a");
		planManager.createPlan(testPlan);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createPlanLongNameTest() {
		char[] chars = new char[129];
		testPlan.setName(new String(chars));
		planManager.createPlan(testPlan);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createPlanShortDetailsTest() {
		testPlan.setDetails("");
		planManager.createPlan(testPlan);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createPlanLongDetailsTest() {
		char[] chars = new char[1025];
		testPlan.setDetails(new String(chars));
		planManager.createPlan(testPlan);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createPlanSmallFeeTest() {
		testPlan.setFee(-1);
		planManager.createPlan(testPlan);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createPlanBigFeeTest() {
		testPlan.setFee(1_000_000);
		planManager.createPlan(testPlan);
	}

	@Test
	public void updatePlanSuccessfullyTest() {
		planManager.createPlan(testPlan);
		testPlan.setName("New plan name");
		planManager.updatePlan(testPlan);
	}

	@Test(expected = IllegalArgumentException.class)
	public void updateInvalidPlanTest() {
		planManager.createPlan(testPlan);
		testPlan.setName("a");
		planManager.updatePlan(testPlan);
	}

	@Test
	public void deletePlanSuccessfullyTest() {
		planManager.removePlan(existentUuid);
	}

	@Test(expected = IllegalArgumentException.class)
	public void deleteNonexistentPlanTest() {
		planManager.removePlan(nonexistentUuid);
	}

	@Test
	public void getPlans() {
		when(dbService.getCustomer(eq(existentUuid))).thenReturn(new Customer().setBalance(100));
		when(dbService.getPlans()).thenReturn(Collections.singletonList(testPlan));
		List<Plan> plans = planManager.getPlans(existentUuid);
		assertEquals(1, plans.size());
		assertEquals(testPlan, plans.get(0));
	}
}