package ru.nsu.fit.shared;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mikhail on 22/11/2018, 11:48;
 */

public class TestUtils {

	public static String extractIdFromResponse(String response) {
		Pattern idPattern = Pattern.compile("^.*\"id\"\\s*:\\s*\"(?<idGroup>[^\"]*)\".*&");
		Matcher matcher = idPattern.matcher(response);
		if (matcher.matches()) {
			return matcher.group("idGroup");
		} else {
			return null;
		}
	}

	public static String extractNameFromResponse(String response) {
		Pattern idPattern = Pattern.compile("^.*\"name\"\\s*:\\s*\"(?<nameGroup>[^\"]*)\".*&");
		Matcher matcher = idPattern.matcher(response);
		if (matcher.matches()) {
			return matcher.group("nameGroup");
		} else {
			return null;
		}
	}

	public static String extractDetailsFromResponse(String response) {
		Pattern idPattern = Pattern.compile("^.*\"details\"\\s*:\\s*\"(?<detailsGroup>[^\"]*)\".*&");
		Matcher matcher = idPattern.matcher(response);
		if (matcher.matches()) {
			return matcher.group("detailsGroup");
		} else {
			return null;
		}
	}

	public static Integer extractFeeFromResponse(String response) {
		Pattern idPattern = Pattern.compile("^.*\"fee\"\\s*:\\s*(?<feeGroup>[\\-\\+0-9]*).*&");
		Matcher matcher = idPattern.matcher(response);
		if (matcher.matches()) {
			String feeString = matcher.group("feeGroup");
			try {
				return Integer.valueOf(feeString);
			} catch (NumberFormatException e) {
				throw new RuntimeException(e);
			}
		} else {
			return null;
		}
	}
}
