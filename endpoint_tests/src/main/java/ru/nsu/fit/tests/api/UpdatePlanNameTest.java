package ru.nsu.fit.tests.api;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.HttpClient;
import ru.nsu.fit.services.HttpMethod;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;

import static org.testng.Assert.assertEquals;

/**
 * Created by mikhail on 22/11/2018, 11:18;
 */

public class UpdatePlanNameTest {

	private HttpClient client;

	private String createdPlanUUID;

	@BeforeClass
	public void initHttpClient() {
		this.client = new HttpClient("http://localhost:8080/endpoint/rest");
	}

	@Test(description = "Create plan via API.")
	public void createPlan() {
		String content = "{\"name\":\"Plan1\",\"details\":\"Details1\",\"fee\":1000}";
		Plan plan = client
				.execute(HttpMethod.POST, "create_plan", content, MediaType.APPLICATION_JSON_TYPE, Plan.class);
		createdPlanUUID = plan.getId().toString();
	}

	@Test(description = "Update created plan with short name via API.", dependsOnMethods = "createPlan")
	public void updatePlanWithShortName() {
		String updatePlanPath = "update_plan/" + createdPlanUUID;
		Plan plan = new Plan("Pl", "Details1", 1000);
		Response response = client.execute(HttpMethod.POST, updatePlanPath, plan);
		assertEquals(400, response.getStatus());
	}

	@Test(description = "Update created plan with long name via API.", dependsOnMethods = "updatePlanWithShortName")
	public void updatePlanWithLongName() {
		String updatePlanPath = "update_plan/" + createdPlanUUID;
		char[] longNameChars = new char[129];
		Arrays.fill(longNameChars, 'a');
		String longName = new String(longNameChars);
		Plan plan = new Plan(longName, "Details1", 1000);
		assertEquals(400, client.execute(HttpMethod.POST, updatePlanPath, plan).getStatus());
	}

	@Test(description = "Update created plan with ok name via API and check it", dependsOnMethods = "updatePlanWithLongName")
	public void updatePlanWithOkName() {
		String updatePlanPath = "update_plan/" + createdPlanUUID;
		Plan plan = new Plan("Plan2", "Details1", 1000);
		Plan newPlan = client.execute(HttpMethod.POST, updatePlanPath, plan, Plan.class);
		assertEquals("Plan2", newPlan.getName());
	}

	@Test(description = "Delete created plan via API.", dependsOnMethods = "updatePlanWithOkName")
	public void deletePlan() {
		String deletePlanPath = "delete_plan/" + createdPlanUUID;
		Response response = client
				.execute(HttpMethod.DELETE, deletePlanPath, null, MediaType.APPLICATION_JSON_TYPE);
		assertEquals(200, response.getStatus());
	}
}
