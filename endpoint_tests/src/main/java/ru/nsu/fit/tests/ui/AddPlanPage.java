package ru.nsu.fit.tests.ui;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.log.LogUtil;
import ru.nsu.fit.tests.api.Plan;

/**
 * Created by mikhail on 2018-12-27, 14:58;
 */

public class AddPlanPage {

	private Browser browser;
	private String errorMessage;

	public AddPlanPage(Browser browser) {
		this.browser = browser;
		this.errorMessage = null;
		browser.openPage("http://localhost:8080/endpoint/add_plan.html");
		browser.waitForElement(By.id("create_plan_id"));
		LogUtil.log("Opened page: http://localhost:8080/endpoint/add_plan.html");
	}

	public String getName() {
		return browser.getElement(By.id("plan_name_id")).getText();
	}

	public void setName(String name) {
		browser.getElement(By.id("plan_name_id")).sendKeys(name);
		LogUtil.log("Set name to" + name);
	}

	public String getDetails() {
		return browser.getElement(By.id("plan_details_id")).getText();
	}

	public void setDetails(String details) {
		browser.getElement(By.id("plan_details_id")).sendKeys(details);
		LogUtil.log("Set details to" + details);
	}

	public String getFee() {
		return browser.getElement(By.id("plan_fee_id")).getText();
	}

	public void setFee(Integer fee) {
		String feeString;
		if (fee < 0) {
			feeString = "";
		} else {
			feeString = fee.toString();
		}
		browser.getElement(By.id("plan_fee_id")).sendKeys(feeString);
		LogUtil.log("Set fee to" + feeString);
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public PlansPage createPlan() {
		LogUtil.log("Click create plan button");
		LogUtil.log(browser.makeScreenshot());
		browser.getElement(By.id("create_plan_id")).click();
		String alertText = browser.getAlertTextIfPresent();
		if (alertText != null) {
			LogUtil.log("Error message " + alertText);
			this.errorMessage = alertText;
			return null;
		}
		return new PlansPage(browser);
	}

	public static void setPlanData(AddPlanPage page, Plan plan) {
		page.setName(plan.getName());
		page.setDetails(plan.getDetails());
		page.setFee(plan.getFee());
	}
}
