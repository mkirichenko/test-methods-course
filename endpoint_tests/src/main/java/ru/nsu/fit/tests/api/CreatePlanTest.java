package ru.nsu.fit.tests.api;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.HttpClient;
import ru.nsu.fit.services.HttpMethod;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.testng.Assert.assertEquals;

/**
 * Created by mikhail on 22/11/2018, 11:18;
 */

public class CreatePlanTest {

	private HttpClient client;

	private String createdPlanUUID;

	@BeforeClass
	public void initHttpClient() {
		this.client = new HttpClient("http://localhost:8080/endpoint/rest");
	}

	@Test(description = "Create plan via API.")
	public void createPlan() {
		String content = "{\"name\":\"Plan1\",\"details\":\"Details1\",\"fee\":1000}";
		Plan plan = client
				.execute(HttpMethod.POST, "create_plan", content, MediaType.APPLICATION_JSON_TYPE, Plan.class);
		createdPlanUUID = plan.getId().toString();
	}

	@Test(description = "Delete created plan via API.", dependsOnMethods = "createPlan")
	public void deletePlan() {
		String deletePlanPath = "delete_plan/" + createdPlanUUID;
		Response response = client
				.execute(HttpMethod.DELETE, deletePlanPath, null, MediaType.APPLICATION_JSON_TYPE);
		assertEquals(200, response.getStatus());
	}
}