package ru.nsu.fit.tests;

import org.junit.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.api.Plan;
import ru.nsu.fit.tests.ui.AddPlanPage;
import ru.nsu.fit.tests.ui.LoginPage;
import ru.nsu.fit.tests.ui.PlansPage;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.util.List;

import static org.hamcrest.core.IsCollectionContaining.hasItem;

public class CreatePlanTest {

    private Browser browser = null;
    private Plan testPlan = new Plan("TestPlan", "TestDetails", 1900);

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @Test
    @Title("Create plan")
    @Description("Create plan via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void createPlan() {
        LoginPage loginPage = new LoginPage(browser);
        loginPage.setLogin("admin");
        loginPage.setPassword("setup");
        PlansPage plansPage = loginPage.login();

        AddPlanPage addPlanPage = plansPage.addPlan();
        AddPlanPage.setPlanData(addPlanPage, testPlan);
        Assert.assertNotNull(addPlanPage.createPlan());
    }

    @Test(dependsOnMethods = "createPlan")
    @Title("Check created plan")
    @Description("Get plan data by name")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void checkPlan() {
        PlansPage plansPage = new PlansPage(browser);
        List<Plan> plans = plansPage.getPlans();

        Assert.assertThat(plans, hasItem(testPlan));
    }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
}
