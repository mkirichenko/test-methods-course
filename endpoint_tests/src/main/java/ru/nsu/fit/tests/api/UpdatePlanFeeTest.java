package ru.nsu.fit.tests.api;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.HttpClient;
import ru.nsu.fit.services.HttpMethod;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.testng.Assert.assertEquals;

/**
 * Created by mikhail on 22/11/2018, 11:18;
 */

public class UpdatePlanFeeTest {

	private HttpClient client;

	private String createdPlanUUID;

	@BeforeClass
	public void initHttpClient() {
		this.client = new HttpClient("http://localhost:8080/endpoint/rest");
	}

	@Test(description = "Create plan via API.")
	public void createPlan() {
		String content = "{\"name\":\"Plan1\",\"details\":\"Details1\",\"fee\":1000}";
		Plan plan = client
				.execute(HttpMethod.POST, "create_plan", content, MediaType.APPLICATION_JSON_TYPE, Plan.class);
		createdPlanUUID = plan.getId().toString();
	}

	@Test(description = "Update created plan with negative fee via API.", dependsOnMethods = "createPlan")
	public void updatePlanWithNegativeFee() {
		String updatePlanPath = "update_plan/" + createdPlanUUID;
		Plan plan = new Plan("Plan1", "Details1", -1);
		Response response = client.execute(HttpMethod.POST, updatePlanPath, plan);
		assertEquals(400, response.getStatus());
	}

	@Test(description = "Update created plan with too big fee via API.", dependsOnMethods = "updatePlanWithNegativeFee")
	public void updatePlanWithTooBigFee() {
		String updatePlanPath = "update_plan/" + createdPlanUUID;
		Plan plan = new Plan("Plan1", "Details1", 1000000);
		Response response = client.execute(HttpMethod.POST, updatePlanPath, plan);
		assertEquals(400, response.getStatus());
	}

	@Test(description = "Update created plan with ok fee via API and check it", dependsOnMethods = "updatePlanWithTooBigFee")
	public void updatePlanWithOkFee() {
		String updatePlanPath = "update_plan/" + createdPlanUUID;
		Plan plan = new Plan("Plan1", "Details1", 2000);
		Plan newPlan = client.execute(HttpMethod.POST, updatePlanPath, plan, Plan.class);
		assertEquals(2000, plan.getFee());
	}

	@Test(description = "Delete created plan via API.", dependsOnMethods = "updatePlanWithOkFee")
	public void deletePlan() {
		String deletePlanPath = "delete_plan/" + createdPlanUUID;
		Response response = client
				.execute(HttpMethod.DELETE, deletePlanPath, null, MediaType.APPLICATION_JSON_TYPE);
		assertEquals(200, response.getStatus());
	}
}
