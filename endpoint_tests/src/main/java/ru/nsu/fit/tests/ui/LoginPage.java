package ru.nsu.fit.tests.ui;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.log.LogUtil;

/**
 * Created by mikhail on 2018-12-27, 14:43;
 */

public class LoginPage {

	private Browser browser;

	public LoginPage(Browser browser) {
		this.browser = browser;
		browser.openPage("http://localhost:8080/endpoint/login.html");
		browser.waitForElement(By.id("email"));
		LogUtil.log("Opened page: http://localhost:8080/endpoint/login.html");
	}

	public String getLogin() {
		return browser.getElement(By.id("email")).getText();
	}

	public void setLogin(String login) {
		browser.getElement(By.id("email")).sendKeys(login);
		LogUtil.log("Set login to " + login);
	}

	public String getPassword() {
		return browser.getElement(By.id("password")).getText();
	}

	public void setPassword(String password) {
		browser.getElement(By.id("password")).sendKeys(password);
		LogUtil.log("Set password to " + password);
	}

	public PlansPage login() {
		LogUtil.log("Click login button");
		LogUtil.log(browser.makeScreenshot());
		browser.getElement(By.id("login")).click();
		return new PlansPage(browser);
	}
}
