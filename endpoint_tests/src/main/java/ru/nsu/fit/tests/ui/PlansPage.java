package ru.nsu.fit.tests.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.log.LogUtil;
import ru.nsu.fit.tests.api.Plan;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mikhail on 2018-12-27, 14:47;
 */

public class PlansPage {

	private Browser browser;

	public PlansPage(Browser browser) {
		this.browser = browser;
		browser.openPage("http://localhost:8080/endpoint/plans.html");
		browser.waitForElement(By.id("add_new_plan"));
		LogUtil.log("Opened page: http://localhost:8080/endpoint/plans.html");
	}

	public AddPlanPage addPlan() {
		LogUtil.log("Click add new plan button");
		LogUtil.log(browser.makeScreenshot());
		browser.getElement(By.id("add_new_plan")).click();
		return new AddPlanPage(browser);
	}

	public List<Plan> getPlans() {
		ArrayList<Plan> plans = new ArrayList<>();
		WebElement planTable = browser.getElement(By.id("plan_list_id"));
		WebElement tableBody = planTable.findElement(By.tagName("tbody"));
		for (WebElement row : tableBody.findElements(By.tagName("tr"))) {
			List<WebElement> cells = row.findElements(By.tagName("td"));
			try {
				String name = cells.get(0).getText();
				String details = cells.get(1).getText();
				String feeText = cells.get(2).getText();
				int fee = Integer.parseInt(feeText);
				plans.add(new Plan(name, details, fee));
			} catch (Exception ignored) {
			}
		}
		LogUtil.log("Retrieve data from plans table");
		LogUtil.log(browser.makeScreenshot());
		return plans;
	}
}
