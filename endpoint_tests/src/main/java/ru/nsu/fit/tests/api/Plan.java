package ru.nsu.fit.tests.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Plan {
    @JsonProperty("id")
    private UUID id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("details")
    private String details;

    @JsonProperty("fee")
    private int fee;

    public Plan() {
    }

    public Plan(String name, String details, int fee) {
        this.name = name;
        this.details = details;
        this.fee = fee;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Plan plan = (Plan)o;

        if (fee != plan.fee) return false;
        if (!name.equals(plan.name)) return false;
        return details.equals(plan.details);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + details.hashCode();
        result = 31 * result + fee;
        return result;
    }

    @Override
    public String toString() {
        return "Plan{" +
            "name='" + name + '\'' +
            ", details='" + details + '\'' +
            ", fee=" + fee +
            '}';
    }
}
