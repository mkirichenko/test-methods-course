package ru.nsu.fit.tests;

import org.junit.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.api.Plan;
import ru.nsu.fit.tests.ui.AddPlanPage;
import ru.nsu.fit.tests.ui.LoginPage;
import ru.nsu.fit.tests.ui.PlansPage;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.util.List;

import static org.hamcrest.core.IsCollectionContaining.hasItem;

public class CreatePlanNameTest {

    private Browser browser = null;
    private Plan testPlan = new Plan("TestPlan", "TestDetails", 1900);

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @Test
    @Title("Login")
    @Description("Login via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void login() {
        LoginPage loginPage = new LoginPage(browser);
        loginPage.setLogin("admin");
        loginPage.setPassword("setup");
        loginPage.login();
    }

    @Test(dependsOnMethods = "login")
    @Title("Create plan without name")
    @Description("Create plan without name via UI")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void createNoNamePlan() {
        PlansPage plansPage = new PlansPage(browser);

        AddPlanPage addPlanPage = plansPage.addPlan();
        Plan noNamePlan = new Plan("", testPlan.getDetails(), testPlan.getFee());
        AddPlanPage.setPlanData(addPlanPage, noNamePlan);
        Assert.assertNull(addPlanPage.createPlan());
        Assert.assertEquals("Name is empty", addPlanPage.getErrorMessage());
    }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
}
