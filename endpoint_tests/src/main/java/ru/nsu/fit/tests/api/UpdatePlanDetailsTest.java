package ru.nsu.fit.tests.api;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.HttpClient;
import ru.nsu.fit.services.HttpMethod;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;

import static org.testng.Assert.assertEquals;

/**
 * Created by mikhail on 22/11/2018, 11:18;
 */

public class UpdatePlanDetailsTest {

	private HttpClient client;

	private String createdPlanUUID;

	@BeforeClass
	public void initHttpClient() {
		this.client = new HttpClient("http://localhost:8080/endpoint/rest");
	}

	@Test(description = "Create plan via API.")
	public void createPlan() {
		String content = "{\"name\":\"Plan1\",\"details\":\"Details1\",\"fee\":1000}";
		Plan plan = client
				.execute(HttpMethod.POST, "create_plan", content, MediaType.APPLICATION_JSON_TYPE, Plan.class);
		createdPlanUUID = plan.getId().toString();
	}

	@Test(description = "Update created plan with short details via API.", dependsOnMethods = "createPlan")
	public void updatePlanWithShortDetails() {
		String updatePlanPath = "update_plan/" + createdPlanUUID;
		Plan plan = new Plan("Plan1", "D", 1000);
		Response response = client.execute(HttpMethod.POST, updatePlanPath, plan);
		assertEquals(400, response.getStatus());
	}

	@Test(description = "Update created plan with long details via API.", dependsOnMethods = "updatePlanWithShortDetails")
	public void updatePlanWithLongDetails() {
		String updatePlanPath = "update_plan/" + createdPlanUUID;
		char[] longDetailsChars = new char[1025];
		Arrays.fill(longDetailsChars, 'a');
		String longDetails = new String(longDetailsChars);
		Plan plan = new Plan("Plan1", longDetails, 1000);
		Response response = client.execute(HttpMethod.POST, updatePlanPath, plan);
		assertEquals(400, response.getStatus());
	}

	@Test(description = "Update created plan with ok details via API and check it", dependsOnMethods = "updatePlanWithLongDetails")
	public void updatePlanWithOkDetails() {
		String updatePlanPath = "update_plan/" + createdPlanUUID;
		Plan plan = new Plan("Plan1", "Details2", 1000);
		Plan newPlan = client.execute(HttpMethod.POST, updatePlanPath, plan, Plan.class);
		assertEquals("Details2", newPlan.getDetails());
	}

	@Test(description = "Delete created plan via API.", dependsOnMethods = "updatePlanWithOkDetails")
	public void deletePlan() {
		String deletePlanPath = "delete_plan/" + createdPlanUUID;
		Response response = client
				.execute(HttpMethod.DELETE, deletePlanPath, null, MediaType.APPLICATION_JSON_TYPE);
		assertEquals(200, response.getStatus());
	}
}
