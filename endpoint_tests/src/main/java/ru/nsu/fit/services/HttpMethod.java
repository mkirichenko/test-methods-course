package ru.nsu.fit.services;

/**
 * Created by mikhail on 2018-12-04, 00:30;
 */

public enum HttpMethod {
	GET,
	POST,
	PUT,
	DELETE;
}
