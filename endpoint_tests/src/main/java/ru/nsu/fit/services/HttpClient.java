package ru.nsu.fit.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import ru.nsu.fit.services.log.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mikhail on 2018-12-04, 00:28;
 */

public class HttpClient {

	private Client client;

	private String baseUrl;

	private ObjectMapper jsonMapper;

	public HttpClient(String baseUrl) {
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(feature);
		clientConfig.register(JacksonFeature.class);
		this.client = ClientBuilder.newClient(clientConfig);
		if (!baseUrl.endsWith("/")) {
			baseUrl += "/";
		}
		this.baseUrl = baseUrl;

		this.jsonMapper = new ObjectMapper();
	}

	public Response execute(HttpMethod method, String path, Object body, MediaType contentType) {
		WebTarget webTarget = client.target(baseUrl).path(path);
		Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
		String content;
		if (body != null) {
			if (body instanceof String) {
				content = (String)body;
			} else {
				try {
					content = jsonMapper.writeValueAsString(body);
				} catch (JsonProcessingException e) {
					throw new RuntimeException(e);
				}
			}
		} else {
			content = null;
		}
		Logger.info(method + " path: " + baseUrl + path + " data: " + content);
		Response response;
		switch (method) {
			case GET:
				response = invocationBuilder.get();
				break;
			case POST:
				response = invocationBuilder.post(Entity.entity(content, contentType));
				break;
			case PUT:
				response = invocationBuilder.put(Entity.entity(content, contentType));
				break;
			case DELETE:
				response = invocationBuilder.delete();
				break;
			default:
				throw new IllegalArgumentException("HttpMethod " + method + " is not supported yes");
		}
		return response;
	}

	public <T> T execute(HttpMethod method, String path, Object body, MediaType contentType, Class<T> returnType) {

		Response response = execute(method, path, body, contentType);
		if (response.getStatus() < 200 || response.getStatus() > 299) {
			String errorBody = response.readEntity(String.class);
			Logger.info(method + " path: " + baseUrl + path + " status: " + response.getStatus() + " response: "
					+ errorBody);
			throw new HttpClientResponseException(response.getStatus(), errorBody);
		}
		T responseBody = response.readEntity(returnType);

		try {
			Logger.info(method + " path: " + baseUrl + path + " status: " + response.getStatus() + " response: "
					+ jsonMapper.writeValueAsString(responseBody));
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
		return responseBody;
	}

	public Response execute(HttpMethod method, String path) {
		return execute(method, path, "", MediaType.APPLICATION_JSON_TYPE);
	}

	public Response execute(HttpMethod method, String path, Object body) {
		return execute(method, path, body, MediaType.APPLICATION_JSON_TYPE);
	}

	public <T> T execute(HttpMethod method, String path, Object body, Class<T> returnType) {
		return execute(method, path, body, MediaType.APPLICATION_JSON_TYPE, returnType);
	}
}
