package ru.nsu.fit.services;

/**
 * Created by mikhail on 2018-12-04, 00:46;
 */

public class HttpClientResponseException extends RuntimeException {

	private final String body;

	private final int status;

	public HttpClientResponseException(int status, String body) {
		super();
		this.status = status;
		this.body = body;
	}

	public String getBody() {
		return body;
	}

	public int getStatus() {
		return status;
	}
}
