package ru.nsu.fit.services.browser;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.nsu.fit.services.log.LogUtil;
import ru.nsu.fit.shared.ImageUtils;

import java.io.Closeable;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Please read: https://github.com/SeleniumHQ/selenium/wiki/Grid2
 *
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Browser implements Closeable {
    private WebDriver webDriver;

    public Browser() {
        // create web driver
        try {
            System.setProperty("webdriver.chrome.driver", "./lib/chromedriver");
            webDriver = new ChromeDriver();
            webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public Browser openPage(String url) {
        webDriver.get(url);
        return this;
    }

    public String getAlertTextIfPresent() {
    	try {
			Alert alert = webDriver.switchTo().alert();
            String text = alert.getText();
            alert.accept();
            LogUtil.log("Alert accepted");
            LogUtil.log(makeScreenshot());
            return text;
		} catch (Exception e) {
    		return null;
		}
	}

    public String getOpenedPage() {
    	return webDriver.getPageSource();
	}

    public Browser waitForElement(By element) {
        return waitForElement(element, 10);
    }

    public Browser waitForElement(final By element, int timeoutSec) {
        WebDriverWait wait = new WebDriverWait(webDriver, timeoutSec);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        return this;
    }

    public Browser click(By element) {
        webDriver.findElement(element).click();
        return this;
    }

    public Browser typeText(By element, String text) {
        webDriver.findElement(element).sendKeys(text);
        return this;
    }

    public WebElement getElement(By element) {
        return webDriver.findElement(element);
    }

    public String getValue(By element) {
        return getElement(element).getAttribute("value");
    }

    public List<WebElement> getElements(By element) {
        return webDriver.findElements(element);
    }

    public boolean isElementPresent(By element) {
        return getElements(element).size() != 0;
    }

    public String makeScreenshot() {
        try {
            byte[] bytes = ImageUtils.toByteArray(((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE));
            String filePath = "test-output/";
            String path = "images/screenshot" + System.currentTimeMillis() + ".png";
            FileOutputStream fileOutputStream = new FileOutputStream(filePath + path);
            fileOutputStream.write(bytes);
            fileOutputStream.close();
            return "<img width=\"500\" height=\"255\" src=\"" + path + "\"/>";
        } catch (IOException ex) {
            ex.printStackTrace();
            return "error";
        }
    }

    @Override
    public void close() {
        webDriver.close();
    }
}
